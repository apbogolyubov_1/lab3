#include <iostream>
#include <map>
#include <fstream>
using namespace std;
template<typename key, typename value,typename weight=bool>
class Graph {
    map<key, value> nodes;
    map<pair<key, key>, weight> connections;//матрица смежности, key1-column, key2-rows
public:
    Graph() = default;
    ~Graph(){
        nodes.clear();
        connections.clear();
    }
    Graph(const Graph<key, value, weight> &other) {
        this->nodes=other.nodes;
        this->connections=other.connections;
    };
    Graph(const Graph<key, value, weight> &&other) {
        this->nodes=move(other.nodes);
        this->connections=move(other.connections);
    };
    Graph& operator=(Graph<key, value, weight>& other){
        if (this!=&other){
            this->nodes=other.nodes;
            this->conne=other.nodes;
        }
        return *this;
    };
    Graph& operator=(const Graph<key, value, weight> &&other){
        if (this!=&other){
            this->nodes=move(other.nodes);
            this->connections=move(other.connections);
        }
        return *this;
    }
    bool empty(){
        return ((nodes.empty())&&(connections.empty()));
    }
    size_t size(){
        return(nodes.size());
    }
    void clear(){
        nodes.clear();
        connections.clear();
    }
    void swap(Graph<key, value, weight> &other){
        swap(nodes, other.nodes);
        swap(connections, other.connections);
    }
    pair<typename map<key, value>::iterator , typename map<pair<key, key>, weight>::iterator> begin(){
        return make_pair(nodes.begin(),connections.begin());
    }
    pair<typename map<key, value>::iterator , typename map<pair<key, key>, weight>::iterator> end(){
        return make_pair(nodes.end(),connections.end());
    }
    pair<typename map<key, value>::const_iterator , typename map<pair<key, key>, weight>::const_iterator> cbegin(){
        return make_pair(nodes.cbegin(),connections.cbegin());
    }
    pair<typename map<key, value>::const_iterator , typename map<pair<key, key>, weight>::const_iterator> cend(){
        return make_pair(nodes.cend(),connections.cend());
    }
    size_t degree_in(key k){
        if (nodes.find(k)==nodes.end()){//contains doesn`t work in my version
            cerr<<"Error, object not found."<<endl;
        }
        size_t s=0;
        for (auto reb:connections){
            if(reb.first.first==k&&reb.second!= 0){//у weight должен быть перегружен оператор != для NULL
                    s+=1;
            }
        }
        return s;
    }
    size_t degree_out(key k){
        if (nodes.find(k)==nodes.end()){
            cerr<<"Error, object not found."<<endl;
        }
        size_t s=0;
        for (auto reb:connections){
            if(reb.first.second==k&&reb.second!=0){
                s+=1;
            }
        }
        return s;
    }
    bool loop(key k){
        if (nodes.find(k)==nodes.end()){
            cerr<<"Error, object not found."<<endl;
        }
        return(connections.find(pair(k,k))!=0);
    }
    pair<typename map<key, value>::iterator, bool> insert_node(key k,value v){
        connections.insert(make_pair(make_pair(k,k),0));
        for (auto node:nodes){
            connections.insert(make_pair(make_pair(node.first,k), 0));
            connections.insert(make_pair(make_pair(k,node.first),0));
        }
        return nodes.insert(make_pair(k,v));
    }
    pair<typename map<key, value>::iterator, bool> insert_or_assign_node(key k,value v){
        weight *s= nullptr;
        connections.insert_or_assign(make_pair(make_pair(k,k),*s));
        for (auto node:nodes){
            connections.insert_or_assign(make_pair(make_pair(node.first,k),*s));
            connections.insert_or_assign(make_pair(make_pair(k,node.first),*s));
        }
        return nodes.insert_or_assign(make_pair(k,v));
    }
    pair<typename map<pair<key, key>, weight>::iterator, bool> insert_edge(key k1, key k2, weight w){
        if (nodes.find(k1)==nodes.end()||nodes.find(k2)==nodes.end()){//contains doesn`t work in my version
            throw "Node not found";
        }
        if (connections.find(make_pair(k1,k2))->second==0){
            connections.find(make_pair(k1,k2))->second=w;
            return make_pair(connections.find(make_pair(k1,k2)), true);
        }
        else{return make_pair(connections.find(make_pair(k1,k2)), false);}
    }
    pair<typename map<pair<key, key>, weight>::iterator, bool> insert_or_assign_edge(key k1,key k2, weight w){
        if (nodes.find(k1)==nodes.end()||nodes.find(k2)==nodes.end()){//contains doesn`t work in my version
            throw "Node not found";
        }
        connections.find(make_pair(k1,k2))->second=w;
        return make_pair(connections.find(make_pair(k1,k2)), true);
    }
    void clear_edges(){
        for(auto con:connections){
            con.second=0;
        }
    }
    bool erase_edges_go_from(key k){
        if (nodes.find(k)==nodes.end()){
            return false;
        }
        for(auto con:connections){
            if(con.first.second==k){
                con.second=0;
            }
        }
        return true;
    }
    bool erase_edges_go_to(key k){
        if (nodes.find(k)==nodes.end()){
            return false;
        }
        for(auto con:connections){
            if(con.first.first==k){
                con.second=0;
            }
        }
        return true;
    }
    bool erase_node(key k){
        if (nodes.find(k)==nodes.end()){
            return false;
        }
        nodes.erase(nodes.find(k));
        for (auto nod:nodes){
            connections.erase(connections.find(make_pair(nod.first,k)));
            connections.erase(connections.find(make_pair(k,nod.first)));
        }
        connections.erase(connections.find(make_pair(k,k)));
    }
    //bool read(ifstream in){
    //    string data;
    //    getline(in,data);
    //}

};
int main(){
    Graph<int,int,int> s;
    Graph<int,int,int> a(s);
    s.insert_node(3,4);
    s.insert_node(4,5);
    s.insert_edge(3,4,1);
    cout<<s.degree_in(3)<<endl;
    cout<<s.degree_in(4)<<endl;
    cout<<s.degree_out(3)<<endl;
    cout<<s.degree_out(4)<<endl;
    s.insert_node(5,6);
    s.insert_node(7,8);
    s.insert_edge(3,5,1);
    s.insert_edge(3,7,1);
    cout<<s.degree_in(3)<<endl;
    s.erase_edges_go_to(3);
    s.erase_edges_go_from(3);
    cout<<s.degree_in(3)<<endl;
return 0;
}