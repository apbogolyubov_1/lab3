#include <iostream>
#include <map>
#include <fstream>
using namespace std;
template<typename key, typename value>
class Graph {
    map<key, pair<value, map<key, int>>> nodes;
public:
    Graph() = default;
    ~Graph(){
        nodes.clear();
    }
    Graph(const Graph<key, value> &other) {
        this->nodes=other.nodes;
    };
    Graph(const Graph<key, value> &&other) {
        this->nodes=move(other.nodes);
    };
    Graph& operator=(Graph<key, value>& other){
        if (this!=&other){
            this->nodes=other.nodes;
        }
        return *this;
    };
    Graph& operator=(const Graph<key, value> &&other){
        if (this!=&other){
            this->nodes=move(other.nodes);
        }
        return *this;
    }
    bool empty(){
        return (nodes.empty());
    }
    size_t size(){
        return(nodes.size());
    }
    void clear() {
        nodes.clear();
    }
    void swap(Graph<key, value> &other){
        swap(nodes, other.nodes);
    }
    typename map<key, pair<value, map<key, int>>>::iterator begin(){
        return nodes.begin();
    }
    typename map<key, pair<value, map<key, int>>>::iterator end(){
        return nodes.end();
    }
    typename map<key, pair<value, map<key, int>>>::const_iterator cbegin(){
        return nodes.cbegin();
    }
    typename map<key, pair<value, map<key, int>>>::const_iterator cend(){
        return nodes.cend();
    }
    size_t degree_in(key k){
        if (nodes.find(k)==nodes.end()){//contains doesn`t work in my version
            cerr<<"Error, object not found."<<endl;
        }
        size_t s=0;
        for (auto node:nodes){
            for(auto reb:node.second.second){
                if(reb.first==k&&reb.second!=0){
                    s+=1;
                }
            }
        }
        return s;
    }
    size_t degree_out(key k){
        if (nodes.find(k)==nodes.end()){//contains doesn`t work in my version
            cerr<<"Error, object not found."<<endl;
        }
        size_t s=0;
        for(auto reb:nodes.at(k).second){
            if(reb.second!=0){
                s+=1;
            }
        }
        return s;
    }
    bool loop(key k){
        if (nodes.find(k)==nodes.end()){
            cerr<<"Error, object not found."<<endl;
        }
        map<key, int> d=nodes.at(k).second;
        for(auto l:d){
            if (l.first==k){
                return true;
            }
        }
        return false;
    }
    pair<typename map<key, pair<value, map<key, int>>>::iterator, bool> insert_node(key k,value v){
        if(nodes.find(k)!=nodes.end()){
            return make_pair(nodes.find(k),false);
        }
        map<key,int> temp;
        for (auto node:nodes){
            temp[node.first]=0;
        }
        nodes[k]= make_pair(v, temp);
        for (auto it=nodes.begin();it!=nodes.end();it++){
            it->second.second[k]=0;
        }
        return make_pair(nodes.find(k),true);
    }
    pair<typename map<key, pair<value, map<key, int>>>::iterator, bool> insert_or_assign_node(key k,value v){
        if(nodes.find(k)!=nodes.end()){
            nodes.at(k).first=v;
            return make_pair(nodes.find(k), false);
        }
        map<key,int> temp;
        for (auto node:nodes){
            temp[node.first]=0;
        }
        nodes[k]= make_pair(v, temp);
        for (auto it=nodes.begin();it!=nodes.end();it++){
            it->second.second[k]=0;
        }
        return make_pair(nodes.find(k),true);
    }
    pair<typename map<key, pair<value, map<key, int>>>::iterator, bool> insert_edge(key k1, key k2, int w){
        if (w==0){
            return make_pair(nodes.find(k1),false);
        }
        if (nodes.find(k1)==nodes.end()||nodes.find(k2)==nodes.end()){//contains doesn`t work in my version
            throw "Node not found";
        }
        if (nodes.at(k1).second.at(k2)!=0){
            return make_pair(nodes.find(k1),false);
        }
        else{
            nodes.at(k1).second.at(k2)=w;
            return make_pair(nodes.find(k1),true);
        }
    }
    pair<typename map<key, pair<value, map<key, int>>>::iterator, bool> insert_or_assign_edge(key k1,key k2, int w){
        if (w==0){
            return make_pair(nodes.find(k1),false);
        }
        if (nodes.find(k1)==nodes.end()||nodes.find(k2)==nodes.end()){//contains doesn`t work in my version
            throw "Node not found";
        }
        nodes.at(k1).second.at(k2)=w;
        return make_pair(nodes.find(k1),true);
    }
    void clear_edges(){
        for (auto it=nodes.begin();it!=nodes.end();it++){
            for (auto ite=it->second.second.begin();ite!=it->second.second.end();ite++){
                ite->second=0;
            }
        }
    }
    bool erase_edges_go_from(key k) {
        if (nodes.find(k) == nodes.end()) {
            return false;
        }
        for (auto it=nodes.at(k).second.begin();it!=nodes.at(k).second.end();it++){
            it->second=0;
        }
        return true;
    }

    bool erase_edges_go_to(key k){
        if (nodes.find(k)==nodes.end()){
            return false;
        }
        for (auto it=nodes.begin();it!=nodes.end();it++){
            it->second.second.at(k)=0;
        }
        return true;
    }
    bool erase_node(key k){
        if (nodes.find(k)==nodes.end()){
            return false;
        }
        nodes.erase(nodes.find(k));
        for (auto it=nodes.begin();it!=nodes.end();it++){
            it->second.second.erase(it->second.second.find(k));
        }
        return true;
    }
    void type(){
        for (auto it=nodes.begin();it!=nodes.end();it++){
            cout<<endl<<it->first<<","<<it->second.first<<":\t";
            for (auto ite=it->second.second.begin();ite!=it->second.second.end();ite++) {
                cout<<"("<<ite->first<<","<<ite->second<<"),\t";
            }
        }
        cout<<endl<<endl;
    }
    void save_to(string str){
        ofstream out;          // поток для записи
        out.open(str); // окрываем файл для записи
        if (out.is_open()) {
            for (auto it = nodes.begin(); it != nodes.end(); it++) {
                out << endl << it->first << "," << it->second.first << ":\t";
                for (auto ite = it->second.second.begin(); ite != it->second.second.end(); ite++) {
                    out << "(" << ite->first << "," << ite->second << "),\t";
                }
            }
            out << endl << endl;
            out.close();
        }
    }
};
int main(){
    Graph<int,int> s;
    Graph<int,int> a(s);
    s.insert_node(1,2);
    s.insert_node(2,3);
    s.insert_node(3,4);
    s.insert_node(4,5);
    s.insert_edge(1,2,2);
    s.insert_edge(1,3,3);
    s.insert_edge(1,4,4);
    s.insert_edge(3,2,2);
    s.insert_edge(4,2,2);
    cout<<s.degree_out(1)<<endl;
    cout<<s.degree_in(2)<<endl;
    s.type();
    s.erase_edges_go_from(3);
    s.erase_edges_go_to(4);
    s.type();
    s.clear_edges();
    s.type();
    s.erase_node(4);
    s.type();
    s.insert_edge(1,3,3);
    s.type();
    s.insert_edge(1,3,30);//не сработает
    s.type();
    s.insert_or_assign_edge(1,3,30);
    s.type();
    s.save_to("C:\\Users\\tadss\\CLionProjects\\untitled12\\just_file.txt");
    return 0;
}